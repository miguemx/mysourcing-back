<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PersonaController;
use App\Http\Controllers\CatalogosController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


/*
CREATE TABLE `mysourcing`.`personas` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la persona' , 
    `nombre` VARCHAR(100) NULL COMMENT 'Nombre de pila' , 
    `primer_apellido` VARCHAR(100) NULL COMMENT 'Primer apellido' , 
    `segundo_apellido` VARCHAR(100) NULL COMMENT 'Segundo apellido' , 
    `email` VARCHAR(100) NOT NULL COMMENT 'Correo electronico ' , 
    `telefono` VARCHAR(10) NULL COMMENT 'Telefono a 10 digitos' , 
    `cp` VARCHAR(5) NULL COMMENT 'Codigo postal a 5 digitos' , 
    `estado` VARCHAR(50) NULL COMMENT 'Estado de la republica' , 
    `created_at` DATETIME NULL COMMENT 'Fecha de creacion' , 
    `updated_at` DATETIME NULL COMMENT 'Ultima actualizacion' , 
    `deleted_at` DATETIME NULL COMMENT 'Fecha de eliminacion' , 
    PRIMARY KEY (`id`), 
    UNIQUE (`email`)
) ENGINE = InnoDB;
*/



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// personas
Route::get( 'personas', [ PersonaController::class, 'lista' ] );
Route::post( 'personas', [ PersonaController::class, 'crea' ] );
Route::delete( 'personas/{id}', [ PersonaController::class, 'elimina' ] );


// lugares
Route::get( 'codigo-postal/{cp}', [ CatalogosController::class, 'estadoPorCp' ] );

// test
Route::get("/test", function() {
    return "test";
});