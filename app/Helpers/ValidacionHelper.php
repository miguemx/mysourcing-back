<?php

namespace App\Helpers;

class ValidacionHelper { 

    private $datos;
    private $errores;

    public function __construct($datos) {
        $this->datos = $datos;
        $this->errores = [];
    }

    public function requerido($campo, $mensaje = null) {
        if (!array_key_exists($campo, $this->datos) || empty($this->datos[$campo])) {
            $this->addError($mensaje ?? ucfirst($campo)." es requerido.");
        }
        return $this;
    }

    public function email($campo, $mensaje = null) {
        if ( !array_key_exists($campo, $this->datos) ) return $this; 
        if ( !preg_match("/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/", $this->datos[$campo]) ) {
            $this->addError($mensaje ?? ucfirst($campo)." debe ser una dirección de correo electrónico.");
        }
        return $this;
    }

    public function nombre($campo, $mensaje = null) {
        if ( !array_key_exists($campo, $this->datos) ) return $this; 
        if ( !preg_match("/^[a-zñ]+( )?[a-zñ]$/i", $this->datos[$campo]) ) {
            $this->addError($mensaje ?? ucfirst($campo)." no coincide con la sintaxis de un nombre convencional.");
        }
        return $this;
    }

    public function telefono($campo, $mensaje = null) {
        if ( !array_key_exists($campo, $this->datos) ) return $this; 
        if ( !preg_match("/^[0-9]{10}$/", $this->datos[$campo]) ) {
            $this->addError($mensaje ?? ucfirst($campo)." debe contener 10 dígitos numéricos.");
        }
        return $this;
    }

    public function getErrores() {
        return $this->errores;
    }

    private function addError($mensaje) {
        $mensaje = str_replace('_', ' ', $mensaje );
        $this->errores[] = $mensaje;
    }

}