<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class Copomex {

    /**
     * busca en la API de COPOMEX un codigo postal y extrae el estado al que pertenece
     * @param cp el codigo postal a buscar
     * @return string cadena con el nombre del estado
     * @return null en caso de no encontrar el estado o tener un error desde la API
     */
    public static function estadoPorCp($cp) {
        $baseUrl = env('COPOMEX_URL');
        $token = env('COPOMEX_TOKEN');
        $url = $baseUrl.$cp.'?type=simplified&token='.$token;
        try {
            $response = Http::get($url);
            if ($response->successful()) {
                $datos = $response->json();
                return $datos['response']['estado'];
            }
        }
        catch ( \Exception $ex ) {
            // TODO
        }
        return null;
    }

}