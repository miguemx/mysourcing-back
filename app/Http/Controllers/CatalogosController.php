<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Copomex;

class CatalogosController extends Controller
{
    /**
     * devuelve la informacion de un estado mediante la consulta de un codigo postal
     * @param cp el codigo postal a buscar
     */
    public function estadoPorCp(Request $request, $cp) {
        if ( !preg_match("/^[0-9]{5}$/",$cp) ) {
            return response()->json( "Código postal no válido", 400 );
        }
        
        $datos = Copomex::estadoPorCp( $cp );
        if ( !is_null($datos) ) {
            return response()->json( $datos, 200 );
        }
        return response()->json( "Código postal no encontrado", 404 );
    }
}
