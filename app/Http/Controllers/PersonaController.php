<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Persona;
use App\Helpers\ValidacionHelper;

class PersonaController extends Controller
{
    
    /**
     * devuelve una lista de los datos de todas las personas registradas
     */
    public function lista(Request $request) {
        $perPage = $request->input('perPage', 10);
        $query = Persona::query();

        if ($request->has('nombre')) { // Filtrar por nombre
            $query->where('nombre', 'LIKE', '%' . $request->input('nombre') . '%');
        }

        if ($request->has('apellidos')) { // Filtrar por apellidos
            $query->where('apellidos', 'LIKE', '%' . $request->input('apellidos') . '%');
        }

        if ($request->has('email')) {  // Filtrar por correo electrónico
            $query->where('email', 'LIKE', '%' . $request->input('email') . '%');
        }

        if ($request->has('orderBy') && $request->has('order')) { // Ordenamiento
            $orderBy = $request->input('orderBy');
            $order = $request->input('order');
            $query->orderBy($orderBy, $order);
        }

        // Obtener los resultados paginados
        $personas = $query->paginate($perPage);
        $personas->appends($request->except('page'));
        return response()->json($personas);
    }

    /**
     * crea un registro de una persona
     */
    public function crea(Request $request) {
        $datos = $request->all();
        try {
            
            $validador = new ValidacionHelper($datos);
            $validador->requerido('nombre')->nombre('nombre')
                    ->requerido('primer_apellido')->nombre('primer_apellido')
                    ->requerido('segundo_apellido')->nombre('segundo_apellido')
                    ->requerido('email')->email('email')
                    ->requerido('telefono')->telefono('telefono');

            $errores = $validador->getErrores();

            if (!empty($errores)) {
                return response()->json( $errores, 400 );
            } 

            $persona = Persona::create( $datos );
            return response()->json( $persona );
        }
        catch ( \Illuminate\Database\UniqueConstraintViolationException $ex ) {
            return response()->json( "El correo ya está en uso", 409 );
        }
        return response()->json( "Recurso no disponible", 404 );
    }

    /**
     * elimina un registro de persona
     * @param id el ID de la persona a eliminar
     */
    public function elimina(Request $request, $id) {
        $persona = Persona::find($id);
        if ( !is_null($persona) ) {
            $persona->delete();
            return response()->json("Registro eliminado", 200);        
        }
        return response()->json("Persona no encontrada", 404);
    }

}
